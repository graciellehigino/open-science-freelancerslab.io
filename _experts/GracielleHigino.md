---
title: "Gracielle Higino"
excerpt: "Building communities and transforming culture through Open Science"
header:
  teaser: assets/images/Gracielle-Higino.png
sidebar:
  - title: "Homepage"
    image: assets/images/Gracielle-Higino.png
    image_alt: "Gracielle"
    text: "[https://graciellehigino.github.io (under renovation)](https://graciellehigino.github.io)"
  - title: "Contact"
    text: "[graciellehigino@gmail.com](mailto:graciellehigino@gmail.com)"
  - title: "Languages"                       
    text: "English, Portuguese"
---
### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Access Publishing, Peer Review, Open Leadership, Open Project Management, Open Data (especially in Ecology and Evolution), Reproducibility, Research Culture, Science Communication, Open Educational Resources.

### Services: 
<!--PLEASE SELECT THE SERVICES THAT YOU OFFER:-->
Speaking, Training (delivery and development), Consultancy, Event Organisation, Research, Mentorship, Community Management, Project Management, science communication strategy and production, Peer Review.

### Research background: 
<!--PLEASE ENTER YOUR AREA OF RESEARCH (e.g. Biomedical Sciences)-->
PhD in Ecology and Evolution.

### Speaker expertise: 
<!--Please enter free text on the talks that you offer or delete if you do not offer talks-->
I have a series of talks about science communication and the interface between open science, science communication and policy. I also offer talks about open science culture, the peer review process, research culture, version control, and more. On-demand speaking services also available.

### Training expertise: 
<!--Please enter free text on the training that you offer or delete if you do not offer training-->
I develop and deliver training in diverse formats focused on reproducibility, version control and open science practices. As an example, I currently contribute to the Living Data Project courses on Data Management, Productivity and Reproducibility, Synthesis in Ecology and Evolution, and Collaboration. I also produce bespoke training on science communication (focused on practice) and principles of data visualization.

### Consultancy expertise: 
<!--Please enter free text on the consultancy services that you offer or delete if you do not offer consultancy-->
I can help you design an open science strategy (from ideation to publication and outreach), train you or your community on open science practices, serve as a community manager, design community engagement activities around open science culture, help you or your community organize preprint peer review clubs, design a working group, and more!
Just need to find a good open access journal to publish your research? Want me to review your dataset to see if it's appropriate for archiving? Not sure if you have all documentation clear for your code? Get in touch!

### Research expertise: 
<!--Please enter free text on research services that you offer or delete if you do not offer research services-->
Peer review in Ecology, Evolution and research culture.

### Event organisation expertise: 
<!--Please enter free text on event organisation services that you offer or delete if you do not offer those services-->
I have experience organizing unorthodox events focused on emerging connections (unconferences, sprints, federated designed events), so if you are looking for something unusual for your community, feel free to reach out!


### Additional info: 
<!--Please enter free text on or delete if you do not want to add additional info-->
Want to chat quickly? Pick a spot on my calendar [here](https://doodle.com/bp/graciellehigino/meet-me)!

City: Vancouver, BC <br/> 
Country: Canada <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel worldwide. <br/>
{: .notice}



