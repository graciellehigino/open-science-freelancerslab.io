---
title: "YOUR NAME"
excerpt: "YOUR SUPER SHORT DESCRIPTION"
header:
  teaser: assets/images/YOUR_PROFILE_PIC.jpeg
sidebar:
  - title: "Homepage"
    image: assets/images/YOUR_PROFILE_PIC.jpeg
    image_alt: "YOUR FIRST NAME"
    text: "[https://YOURWEBSITE.com](https://YOURWEBSITE.com)"
  - title: "Contact"
    text: "[YOUR@EMAIL.COM](mailto:YOUR@EMAIL.COM)"
  - title: "Languages"                       
    text: "THE LANGUAGES YOU SPEAK/ OFFER SERVICES IN"
---
### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Access Publishing, Open Data/ Methods, Open Source, Citizen Science, Open Innovation, Open Educational Resources, Reproducibility/ Replicability, Research Software Engineering, Research Culture, Research(er) Assessment, Equity/ Diversity/ Inclusivity in Research

### Services: 
<!--PLEASE SELECT THE SERVICES THAT YOU OFFER:-->
Speaker, Training, Consultancy, Research, Event Moderation, Event Organisation

### Research background: 
<!--PLEASE ENTER YOUR AREA OF RESEARCH (e.g. Biomedical Sciences)-->

### Speaker expertise: 
<!--Please enter free text on the talks that you offer or delete if you do not offer talks-->

### Training expertise: 
<!--Please enter free text on the training that you offer or delete if you do not offer training-->

### Consultancy expertise: 
<!--Please enter free text on the consultancy services that you offer or delete if you do not offer consultancy-->

### Research expertise: 
<!--Please enter free text on research services that you offer or delete if you do not offer research services-->

### Event organisation expertise: 
<!--Please enter free text on event organisation services that you offer or delete if you do not offer those services-->

### Event moderation expertise: 
<!--Please enter free text on event moderation services that you offer or delete if you do not offer event moderation-->

### Additional info: 
<!--Please enter free text on or delete if you do not want to add additional info-->

City: WHERE YOU ARE BASED <br/> 
Country: WHERE YOU ARE BASED <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel in YOUR HOME COUNTRY/ YOUR HOME CONTINENT/ WORLDWIDE <br/>
{: .notice}



